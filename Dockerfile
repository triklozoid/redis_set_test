FROM ubuntu:16.04
ENV PYTHONUNBUFFERED 1

RUN mkdir /code
WORKDIR /code

RUN apt-get update -y

RUN apt-get install python3 python3-pip -y

RUN apt-get install python3-redis -y

RUN apt-get install redis-tools -y

