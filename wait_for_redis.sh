#!/bin/bash

set -e

cmd="$@"

until [ `redis-cli -h redis -p 6379 ping|grep -c PONG` = 1 ]; do
  >&2 echo "Redis is loading - sleeping"
  sleep 1
done

>&2 echo "Redis is up - executing command"
exec $cmd
