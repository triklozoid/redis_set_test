#!/usr/bin/env python3

import redis
import timeit
import uuid

r = redis.StrictRedis(host='redis', port=6379, db=0)

for v in range(1, 100):
    r.sadd("set_100", uuid.uuid4())

for v in range(1, 1000000):
    r.sadd("set_1000000", uuid.uuid4())

for v in range(1, 10000000):
    r.sadd("set_10000000", uuid.uuid4())


randmember = r.srandmember('set_100')
print(randmember)
print(timeit.timeit("r.sismember('set_100', %s)" % randmember, setup="import redis;r = redis.StrictRedis(host='redis', port=6379, db=0)", number=10000))

randmember = r.srandmember('set_100')
print(randmember)
print(timeit.timeit("r.sismember('set_1000000', %s)" % randmember, setup="import redis;r = redis.StrictRedis(host='redis', port=6379, db=0)", number=10000))

randmember = r.srandmember('set_100')
print(randmember)
print(timeit.timeit("r.sismember('set_10000000', %s)" % randmember, setup="import redis;r = redis.StrictRedis(host='redis', port=6379, db=0)", number=10000))

